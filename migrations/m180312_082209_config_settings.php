<?php

use yii\db\Migration;
use skinka\yii2\extension\config\Config;

/**
 * Class m180312_082209_config_settings
 */
class m180312_082209_config_settings extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180312_082209_config_settings cannot be reverted.\n";

        return false;
    }

    public function up()
    {
        Config::setNew('adminEmail', 'Administrator email', 'admin@site.com',
            Config::TYPE_STRING, Config::INPUT_INPUT, [['email']], [], '', 0);

        Config::setNew('dateTimeFormat', 'Datetime format for site', 'php:d.m.Y H:i:s',
            Config::TYPE_STRING, Config::INPUT_INPUT, [['string']], [],
            'Date in PHP format. All formats can be seen here: http://php.net/manual/en/function.date.php', 1);

        Config::setNew('autoConfirmRegistration', 'Automatic registration', true,
            Config::TYPE_BOOLEAN, Config::INPUT_DROPDOWN, [['integer']], [0 => 'Off', 1 => 'On'],
            'If enabled, the user at the email will not receive a notification of the activation', 2);

        //Others
    }

    public function down()
    {
        Config::delete('adminEmail');
        Config::delete('dateTimeFormat');
        Config::delete('autoConfirmRegistration');

        //Others
    }
}
