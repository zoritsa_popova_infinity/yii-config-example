<?php
// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/../config/web.php';

$app = new yii\web\Application($config);

$dbParams = skinka\yii2\extension\config\ConfigModel::find()->select(['name', 'value'])->asArray()->all();

if(!empty($dbParams)){
    foreach ($dbParams as $dbParam){
        $app->params[$dbParam['name']] = $dbParam['value'];
    }
}

$app->run();


